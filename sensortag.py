import pexpect
import sys
import Adafruit_CharLCD as LCD
from sys import stdin
import MySQLdb
import time
import datetime

#from sensor_calcs import *
#import json
#import select

lcd = LCD.Adafruit_CharLCDPlate()
lcd.set_color(1.0, 1.0, 1.0)


def tmp006( v ):

 temp_bytes = v.split() # Split into individual bytes
 raw_ambient_temp = int( '0x'+ temp_bytes[3]+ temp_bytes[2], 16) # Choose ambient temperature (reverse bytes for little endian)
 ambient_temp_int = raw_ambient_temp >> 2 & 0x3FFF # Shift right, based on from TI
 ambient_temp_celsius = float(ambient_temp_int) * 0.03125 # Convert to Celsius based on info from TI
 temp = str(ambient_temp_celsius)
 databaseparse(1,ambient_temp_celsius)
 showtemp(temp)

def inittmp006 ():
 con = pexpect.spawn('gatttool -b ' + 'B0:B4:48:BD:9D:00' + ' --interactive')
 con.expect('\[LE\]>')#, timeout=600)
# print "Preparing to connect. You might need to press the side button..."
 con.sendline('connect')
 # test for success of connect
 con.expect('Connection successful.*\[LE\]>')
 con.sendline('char-write-cmd 0x24 01')
 con.sendline('char-read-hnd 0x21')
 con.expect('descriptor: .*? \r')
 after = con.after
 rval = after.split()
 temp = after.replace("descriptor:","")
# print after
 tmp006(temp)
# con.sendline('char-write-cmd 0x24 00')

def hdc1000( v ):
 
#print str
 v.split(' ')
 test = "0x" + v[10] + v[1] + v[7] + v[8]
 #rawtemp = int("0x9cec",16)
 rawhum = int(test,16)
 floattest = float(rawhum)
 hum = float(((floattest / 65536)*100))
 databaseparse(2,hum)
 humtest = str(hum)
 showhum( humtest)

def inithdc1000 ():
 con = pexpect.spawn('gatttool -b ' + 'B0:B4:48:BD:9D:00' + ' --interactive')
 con.expect('\[LE\]>', timeout=600)
# print "Preparing to connect. You might need to press the side button..."
 con.sendline('connect')
 # test for success of connect
 con.expect('Connection successful.*\[LE\]>')
 con.sendline('char-write-cmd 0x2c 01')
 con.sendline('char-read-hnd 0x29')
 con.expect('descriptor: .*? \r')
 after = con.after
 rval = after.split()
 hum = after.replace("descriptor:","")
 #print hum
 hdc1000(hum)
# con.sendline('char-write-cmd 0x24 00')

def showtemp( str ):
 lcd.clear()
 lcd.message("Temperature: " + "\n" +  str)



def showhum( str ):
 lcd.clear()
 lcd.message("Humidity: " + "\n" +  str)


def databaseparse ( type, value ):
 ts = time.time()
 timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
 conn = MySQLdb.connect(host= "localhost",
                  user="root",
                  passwd="1",
                  db="EmbeddedSystems")
 x = conn.cursor()

 try:
   x.execute("""INSERT INTO sensortag VALUES (NULL,%s,%s,%s)""",(type,value,timestamp))
   conn.commit()
 except:
   conn.rollback()

 conn.close()

#inittmp006()
input = raw_input('Enter your input:')
while 1 :
 if input == 'tmp':
  time.sleep(1.0) 
  inittmp006()
  time.sleep(1.0)
  inithdc1000()
	



